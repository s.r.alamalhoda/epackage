const treeSource = [{
    icon: '../../images/earth.png',
    label: 'Project',
    expanded: true,
    items: [{
        icon: '../../images/folder.png',
        label: 'css',
        expanded: true,
        items: [{
            icon: '../../images/nav1.png',
            label: 'jqx.base.css'
        }, {
            icon: '../../images/nav1.png',
            label: 'jqx.energyblue.css'
        }, {
            icon: '../../images/nav1.png',
            label: 'jqx.orange.css'
        }]
    }, {
        icon: '../../images/folder.png',
        label: 'scripts',
        items: [{
            icon: '../../images/nav1.png',
            label: 'jqxcore.js'
        }, {
            icon: '../../images/nav1.png',
            label: 'jqxdata.js'
        }, {
            icon: '../../images/nav1.png',
            label: 'jqxgrid.js'
        }]
    }, {
        icon: '../../images/nav1.png',
        label: 'index.htm'
    }]
}];

/////////////
// const countries = ["Afghanistan", "Albania", "Algeria", "Andorra", "Angola", "Antarctica", "Antigua and Barbuda", "Argentina", "Armenia", "Australia", "Austria", "Azerbaijan", "Bahamas", "Bahrain", "Bangladesh", "Barbados", "Belarus", "Belgium", "Belize", "Benin", "Bermuda", "Bhutan", "Bolivia", "Bosnia and Herzegovina", "Botswana", "Brazil", "Brunei", "Bulgaria", "Burkina Faso", "Burma", "Burundi", "Cambodia", "Cameroon", "Canada", "Cape Verde", "Central African Republic", "Chad", "Chile", "China", "Colombia", "Comoros", "Congo, Democratic Republic", "Congo, Republic of the", "Costa Rica", "Cote d'Ivoire", "Croatia", "Cuba", "Cyprus", "Czech Republic", "Denmark", "Djibouti", "Dominica", "Dominican Republic", "East Timor", "Ecuador", "Egypt", "El Salvador", "Equatorial Guinea", "Eritrea", "Estonia", "Ethiopia", "Fiji", "Finland", "France", "Gabon", "Gambia", "Georgia", "Germany", "Ghana", "Greece", "Greenland", "Grenada", "Guatemala", "Guinea", "Guinea-Bissau", "Guyana", "Haiti", "Honduras", "Hong Kong", "Hungary", "Iceland", "India", "Indonesia", "Iran", "Iraq", "Ireland", "Israel", "Italy", "Jamaica", "Japan", "Jordan", "Kazakhstan", "Kenya", "Kiribati", "Korea, North", "Korea, South", "Kuwait", "Kyrgyzstan", "Laos", "Latvia", "Lebanon", "Lesotho", "Liberia", "Libya", "Liechtenstein", "Lithuania", "Luxembourg", "Macedonia", "Madagascar", "Malawi", "Malaysia", "Maldives", "Mali", "Malta", "Marshall Islands", "Mauritania", "Mauritius", "Mexico", "Micronesia", "Moldova", "Mongolia", "Morocco", "Monaco", "Mozambique", "Namibia", "Nauru", "Nepal", "Netherlands", "New Zealand", "Nicaragua", "Niger", "Nigeria", "Norway", "Oman", "Pakistan", "Panama", "Papua New Guinea", "Paraguay", "Peru", "Philippines", "Poland", "Portugal", "Qatar", "Romania", "Russia", "Rwanda", "Samoa", "San Marino", " Sao Tome", "Saudi Arabia", "Senegal", "Serbia and Montenegro", "Seychelles", "Sierra Leone", "Singapore", "Slovakia", "Slovenia", "Solomon Islands", "Somalia", "South Africa", "Spain", "Sri Lanka", "Sudan", "Suriname", "Swaziland", "Sweden", "Switzerland", "Syria", "Taiwan", "Tajikistan", "Tanzania", "Thailand", "Togo", "Tonga", "Trinidad and Tobago", "Tunisia", "Turkey", "Turkmenistan", "Uganda", "Ukraine", "United Arab Emirates", "United Kingdom", "United States", "Uruguay", "Uzbekistan", "Vanuatu", "Venezuela", "Vietnam", "Yemen", "Zambia", "Zimbabwe"];

const countries =
    [
        {
            "id": 0,
            "name": "Afghanistan"
        },
        {
            "id": 1,
            "name": "Albania"
        },
        {
            "id": 2,
            "name": "Algeria"
        },
        {
            "id": 3,
            "name": "Andorra"
        },
        {
            "id": 4,
            "name": "Angola"
        },
        {
            "id": 5,
            "name": "Antarctica"
        },
        {
            "id": 6,
            "name": "Antigua and Barbuda"
        },
        {
            "id": 7,
            "name": "Argentina"
        },
        {
            "id": 8,
            "name": "Armenia"
        },
        {
            "id": 9,
            "name": "Australia"
        },
        {
            "id": 10,
            "name": "Austria"
        },
        {
            "id": 11,
            "name": "Azerbaijan"
        },
        {
            "id": 12,
            "name": "Bahamas"
        },
        {
            "id": 13,
            "name": "Bahrain"
        },
        {
            "id": 14,
            "name": "Bangladesh"
        },
        {
            "id": 15,
            "name": "Barbados"
        },
        {
            "id": 16,
            "name": "Belarus"
        },
        {
            "id": 17,
            "name": "Belgium"
        },
        {
            "id": 18,
            "name": "Belize"
        },
        {
            "id": 19,
            "name": "Benin"
        },
        {
            "id": 20,
            "name": "Bermuda"
        },
        {
            "id": 21,
            "name": "Bhutan"
        },
        {
            "id": 22,
            "name": "Bolivia"
        },
        {
            "id": 23,
            "name": "Bosnia and Herzegovina"
        },
        {
            "id": 24,
            "name": "Botswana"
        },
        {
            "id": 25,
            "name": "Brazil"
        },
        {
            "id": 26,
            "name": "Brunei"
        },
        {
            "id": 27,
            "name": "Bulgaria"
        },
        {
            "id": 28,
            "name": "Burkina Faso"
        },
        {
            "id": 29,
            "name": "Burma"
        },
        {
            "id": 30,
            "name": "Burundi"
        },
        {
            "id": 31,
            "name": "Cambodia"
        },
        {
            "id": 32,
            "name": "Cameroon"
        },
        {
            "id": 33,
            "name": "Canada"
        },
        {
            "id": 34,
            "name": "Cape Verde"
        },
        {
            "id": 35,
            "name": "Central African Republic"
        },
        {
            "id": 36,
            "name": "Chad"
        },
        {
            "id": 37,
            "name": "Chile"
        },
        {
            "id": 38,
            "name": "China"
        },
        {
            "id": 39,
            "name": "Colombia"
        },
        {
            "id": 40,
            "name": "Comoros"
        },
        {
            "id": 41,
            "name": "Congo, Democratic Republic"
        },
        {
            "id": 42,
            "name": "Congo, Republic of the"
        },
        {
            "id": 43,
            "name": "Costa Rica"
        },
        {
            "id": 44,
            "name": "Cote d'Ivoire"
        },
        {
            "id": 45,
            "name": "Croatia"
        },
        {
            "id": 46,
            "name": "Cuba"
        },
        {
            "id": 47,
            "name": "Cyprus"
        },
        {
            "id": 48,
            "name": "Czech Republic"
        },
        {
            "id": 49,
            "name": "Denmark"
        },
        {
            "id": 50,
            "name": "Djibouti"
        },
        {
            "id": 51,
            "name": "Dominica"
        },
        {
            "id": 52,
            "name": "Dominican Republic"
        },
        {
            "id": 53,
            "name": "East Timor"
        },
        {
            "id": 54,
            "name": "Ecuador"
        },
        {
            "id": 55,
            "name": "Egypt"
        },
        {
            "id": 56,
            "name": "El Salvador"
        },
        {
            "id": 57,
            "name": "Equatorial Guinea"
        },
        {
            "id": 58,
            "name": "Eritrea"
        },
        {
            "id": 59,
            "name": "Estonia"
        },
        {
            "id": 60,
            "name": "Ethiopia"
        },
        {
            "id": 61,
            "name": "Fiji"
        },
        {
            "id": 62,
            "name": "Finland"
        },
        {
            "id": 63,
            "name": "France"
        },
        {
            "id": 64,
            "name": "Gabon"
        },
        {
            "id": 65,
            "name": "Gambia"
        },
        {
            "id": 66,
            "name": "Georgia"
        },
        {
            "id": 67,
            "name": "Germany"
        },
        {
            "id": 68,
            "name": "Ghana"
        },
        {
            "id": 69,
            "name": "Greece"
        },
        {
            "id": 70,
            "name": "Greenland"
        },
        {
            "id": 71,
            "name": "Grenada"
        },
        {
            "id": 72,
            "name": "Guatemala"
        },
        {
            "id": 73,
            "name": "Guinea"
        },
        {
            "id": 74,
            "name": "Guinea-Bissau"
        },
        {
            "id": 75,
            "name": "Guyana"
        },
        {
            "id": 76,
            "name": "Haiti"
        },
        {
            "id": 77,
            "name": "Honduras"
        },
        {
            "id": 78,
            "name": "Hong Kong"
        },
        {
            "id": 79,
            "name": "Hungary"
        },
        {
            "id": 80,
            "name": "Iceland"
        },
        {
            "id": 81,
            "name": "India"
        },
        {
            "id": 82,
            "name": "Indonesia"
        },
        {
            "id": 83,
            "name": "Iran"
        },
        {
            "id": 84,
            "name": "Iraq"
        },
        {
            "id": 85,
            "name": "Ireland"
        },
        {
            "id": 86,
            "name": "Israel"
        },
        {
            "id": 87,
            "name": "Italy"
        },
        {
            "id": 88,
            "name": "Jamaica"
        },
        {
            "id": 89,
            "name": "Japan"
        },
        {
            "id": 90,
            "name": "Jordan"
        },
        {
            "id": 91,
            "name": "Kazakhstan"
        },
        {
            "id": 92,
            "name": "Kenya"
        },
        {
            "id": 93,
            "name": "Kiribati"
        },
        {
            "id": 94,
            "name": "Korea, North"
        },
        {
            "id": 95,
            "name": "Korea, South"
        },
        {
            "id": 96,
            "name": "Kuwait"
        },
        {
            "id": 97,
            "name": "Kyrgyzstan"
        },
        {
            "id": 98,
            "name": "Laos"
        },
        {
            "id": 99,
            "name": "Latvia"
        },
        {
            "id": 100,
            "name": "Lebanon"
        },
        {
            "id": 101,
            "name": "Lesotho"
        },
        {
            "id": 102,
            "name": "Liberia"
        },
        {
            "id": 103,
            "name": "Libya"
        },
        {
            "id": 104,
            "name": "Liechtenstein"
        },
        {
            "id": 105,
            "name": "Lithuania"
        },
        {
            "id": 106,
            "name": "Luxembourg"
        },
        {
            "id": 107,
            "name": "Macedonia"
        },
        {
            "id": 108,
            "name": "Madagascar"
        },
        {
            "id": 109,
            "name": "Malawi"
        },
        {
            "id": 110,
            "name": "Malaysia"
        },
        {
            "id": 111,
            "name": "Maldives"
        },
        {
            "id": 112,
            "name": "Mali"
        },
        {
            "id": 113,
            "name": "Malta"
        },
        {
            "id": 114,
            "name": "Marshall Islands"
        },
        {
            "id": 115,
            "name": "Mauritania"
        },
        {
            "id": 116,
            "name": "Mauritius"
        },
        {
            "id": 117,
            "name": "Mexico"
        },
        {
            "id": 118,
            "name": "Micronesia"
        },
        {
            "id": 119,
            "name": "Moldova"
        },
        {
            "id": 120,
            "name": "Mongolia"
        },
        {
            "id": 121,
            "name": "Morocco"
        },
        {
            "id": 122,
            "name": "Monaco"
        },
        {
            "id": 123,
            "name": "Mozambique"
        },
        {
            "id": 124,
            "name": "Namibia"
        },
        {
            "id": 125,
            "name": "Nauru"
        },
        {
            "id": 126,
            "name": "Nepal"
        },
        {
            "id": 127,
            "name": "Netherlands"
        },
        {
            "id": 128,
            "name": "New Zealand"
        },
        {
            "id": 129,
            "name": "Nicaragua"
        },
        {
            "id": 130,
            "name": "Niger"
        },
        {
            "id": 131,
            "name": "Nigeria"
        },
        {
            "id": 132,
            "name": "Norway"
        },
        {
            "id": 133,
            "name": "Oman"
        },
        {
            "id": 134,
            "name": "Pakistan"
        },
        {
            "id": 135,
            "name": "Panama"
        },
        {
            "id": 136,
            "name": "Papua New Guinea"
        },
        {
            "id": 137,
            "name": "Paraguay"
        },
        {
            "id": 138,
            "name": "Peru"
        },
        {
            "id": 139,
            "name": "Philippines"
        },
        {
            "id": 140,
            "name": "Poland"
        },
        {
            "id": 141,
            "name": "Portugal"
        },
        {
            "id": 142,
            "name": "Qatar"
        },
        {
            "id": 143,
            "name": "Romania"
        },
        {
            "id": 144,
            "name": "Russia"
        },
        {
            "id": 145,
            "name": "Rwanda"
        },
        {
            "id": 146,
            "name": "Samoa"
        },
        {
            "id": 147,
            "name": "San Marino"
        },
        {
            "id": 148,
            "name": " Sao Tome"
        },
        {
            "id": 149,
            "name": "Saudi Arabia"
        },
        {
            "id": 150,
            "name": "Senegal"
        },
        {
            "id": 151,
            "name": "Serbia and Montenegro"
        },
        {
            "id": 152,
            "name": "Seychelles"
        },
        {
            "id": 153,
            "name": "Sierra Leone"
        },
        {
            "id": 154,
            "name": "Singapore"
        },
        {
            "id": 155,
            "name": "Slovakia"
        },
        {
            "id": 156,
            "name": "Slovenia"
        },
        {
            "id": 157,
            "name": "Solomon Islands"
        },
        {
            "id": 158,
            "name": "Somalia"
        },
        {
            "id": 159,
            "name": "South Africa"
        },
        {
            "id": 160,
            "name": "Spain"
        },
        {
            "id": 161,
            "name": "Sri Lanka"
        },
        {
            "id": 162,
            "name": "Sudan"
        },
        {
            "id": 163,
            "name": "Suriname"
        },
        {
            "id": 164,
            "name": "Swaziland"
        },
        {
            "id": 165,
            "name": "Sweden"
        },
        {
            "id": 166,
            "name": "Switzerland"
        },
        {
            "id": 167,
            "name": "Syria"
        },
        {
            "id": 168,
            "name": "Taiwan"
        },
        {
            "id": 169,
            "name": "Tajikistan"
        },
        {
            "id": 170,
            "name": "Tanzania"
        },
        {
            "id": 171,
            "name": "Thailand"
        },
        {
            "id": 172,
            "name": "Togo"
        },
        {
            "id": 173,
            "name": "Tonga"
        },
        {
            "id": 174,
            "name": "Trinidad and Tobago"
        },
        {
            "id": 175,
            "name": "Tunisia"
        },
        {
            "id": 176,
            "name": "Turkey"
        },
        {
            "id": 177,
            "name": "Turkmenistan"
        },
        {
            "id": 178,
            "name": "Uganda"
        },
        {
            "id": 179,
            "name": "Ukraine"
        },
        {
            "id": 180,
            "name": "United Arab Emirates"
        },
        {
            "id": 181,
            "name": "United Kingdom"
        },
        {
            "id": 182,
            "name": "United States"
        },
        {
            "id": 183,
            "name": "Uruguay"
        },
        {
            "id": 184,
            "name": "Uzbekistan"
        },
        {
            "id": 185,
            "name": "Vanuatu"
        },
        {
            "id": 186,
            "name": "Venezuela"
        },
        {
            "id": 187,
            "name": "Vietnam"
        },
        {
            "id": 188,
            "name": "Yemen"
        },
        {
            "id": 189,
            "name": "Zambia"
        },
        {
            "id": 190,
            "name": "Zimbabwe"
        },

    ];
function generatedata() {

    // prepare the data
    var data = [];
    // const firstNames = countries;
    rowscount = countries.length;

    for (var i = 0; i < rowscount; i++) {
        var row = {};
        row["id"] = countries[i]['id'];
        row["name"] = countries[i]['name'];
        data[i] = row;
    }
    // console.log('countries.lengh : ' + countries.length);
    // console.log(data);
    return data;
}


function generatedataForGrid(){

    var data =generatedata();
    var source =
        {
            localdata: data,
            datafields:
                [
                    { name: 'id', type: 'string' },
                    { name: 'name', type: 'string' },
                ],
            datatype: "array"
        };
    return new $.jqx.dataAdapter(source);

}
